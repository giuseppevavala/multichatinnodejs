/**
 * Module dependencies.
 */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser')
var cookieParser = require('coockie-parser')
var session = require ('express-session')
var errorHandler = require ('error-handler')

var controller = require('./routes/controller')


// Setting environment
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use('/static', express.static(__dirname + '/public'));
app.use(bodyParser.json())
app.use(cookieParser());
app.use(session({secret: 'lamiapasswordsegreta'}));



// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

//Lista degli entry point
app.get('/', controller.index);
app.post('/login', controller.login);


// WebSocket
io.sockets.on('connection', function (socket) {
	socket.on('sendMsg', function(msg){
		console.log('Messaggio: ' + msg);
		socket.broadcast.emit('newMsg', msg);
	});
});
		  
// Server listen
http.listen(8080, function(){
  console.log('listening on *:8080');
});