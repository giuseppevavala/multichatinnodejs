var socket = io.connect();

socket.on('newMsg', function(data){
	addRecMessage(data);
  });

function btnSendPressed(){
	sendMessage($('#btn-input').val());
	$("#btn-input").val("");
};

function sendMessage(msg){
	addSendMessage(msg);
	socket.emit('sendMsg', msg);
}


function addSendMessage(msg){
	
	var chat = document.getElementById("myChat");
	var tmp = document.getElementById('sendMessageTemplate');
	var msgElem = tmp.cloneNode(true);
	
	msgElem.style.display ="";
	msgElem.getElementsByTagName("p")[0].innerHTML = msg;
	chat.appendChild (msgElem);
	goToLastMessage();
};



function addRecMessage(msg){
	
	var chat = document.getElementById("myChat");
	var tmp = document.getElementById('recMessageTemplate');
	var msgElem = tmp.cloneNode(true);
	
	msgElem.style.display ="";
	msgElem.getElementsByTagName("p")[0].innerHTML = msg;
	chat.appendChild (msgElem);
	goToLastMessage();
};



function goToLastMessage(){
	myChat.scrollTop = myChat.scrollHeight;
}
